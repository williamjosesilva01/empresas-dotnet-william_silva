﻿using System.ComponentModel.DataAnnotations;

namespace empresas_dotNET.DAL.Entities
{
    public class UserAccess
    {
        [Key]
        public int Id { get; set; }
        public int IdInvestor { get; set; }
        public string AccessToken { get; set; }
        public string Client { get; set; }
        public string UID { get; set; }
    }
}
