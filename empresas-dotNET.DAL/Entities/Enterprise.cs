﻿using System;
using System.ComponentModel.DataAnnotations;

namespace empresas_dotNET.DAL.Entities
{
    public class Enterprise
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public string Address { get; set; }
    }
}
