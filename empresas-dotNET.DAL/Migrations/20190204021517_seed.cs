﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace empresas_dotNET.DAL.Migrations
{
    public partial class seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
           table: "enterprise",
           columns: new[] { "Name", "Type", "Address" },
           values: new object[] { "IOSYS", 1, "Rua João da Silva Santos bairro novo número 36" });

            migrationBuilder.InsertData(
           table: "enterprise",
           columns: new[] { "Name", "Type", "Address" },
           values: new object[] { "SQUADRA", 1, "Rua João da Silva Santos bairro novo número 36" });

            migrationBuilder.InsertData(
           table: "enterprise",
           columns: new[] { "Name", "Type", "Address" },
           values: new object[] { "CODEME", 1, "Rua João da Silva Santos bairro novo número 36" });

            migrationBuilder.InsertData(
           table: "enterprise",
           columns: new[] { "Name", "Type", "Address" },
           values: new object[] { "CI&T", 2, "Rua João da Silva Santos bairro novo número 36" });

            migrationBuilder.InsertData(
           table: "enterprise",
           columns: new[] { "Name", "Type", "Address" },
           values: new object[] { "Stefanini IT ", 2, "Rua João da Silva Santos bairro novo número 36" });

            migrationBuilder.InsertData(
           table: "enterprise",
           columns: new[] { "Name", "Type", "Address" },
           values: new object[] { "GOOGLE", 2, "Rua João da Silva Santos bairro novo número 36" });

            migrationBuilder.InsertData(
           table: "enterprise",
           columns: new[] { "Name", "Type", "Address" },
           values: new object[] { "UOL", 3, "Rua João da Silva Santos bairro novo número 36" });

            migrationBuilder.InsertData(
           table: "enterprise",
           columns: new[] { "Name", "Type", "Address" },
           values: new object[] { "GLOBO", 3, "Rua João da Silva Santos bairro novo número 36" });

            migrationBuilder.InsertData(
            table: "enterprise",
            columns: new[] { "Name", "Type", "Address" },
            values: new object[] { "MUNDO", 3, "Rua João da Silva Santos bairro novo número 36" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
