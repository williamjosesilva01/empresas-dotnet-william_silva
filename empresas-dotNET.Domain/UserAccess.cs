﻿using System;

namespace empresas_dotNET.Domain
{
    public class UserAccess
    {
        public string AccessToken { get; set; }
        public string Client { get; set; }
        public string UID { get; set; }
    }
}
