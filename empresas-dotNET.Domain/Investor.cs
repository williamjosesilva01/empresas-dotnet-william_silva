﻿namespace empresas_dotNET.Domain
{
    public class Investor
    {
        public int Id { get; set; }
    }

    public class InvestorData
    {

        public Investor Investor { get; set; }
        public bool Success { get; set; }
    }
}
