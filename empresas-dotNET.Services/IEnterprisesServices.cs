﻿using empresas_dotNET.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace empresas_dotNET.Services
{
    public interface IEnterprisesServices
    {
        List<Enterprise> Get(int? type, string name);
        Enterprise Get(int id);
    }
}
