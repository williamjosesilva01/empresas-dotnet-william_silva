﻿using empresas_dotNET.CrossCuting;
using empresas_dotNET.DAL;
using empresas_dotNET.DAL.Entities;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace empresas_dotNET.Services
{
    public class UserServices : IUserServices
    {
        private readonly ApplicationDbContext _context;

        public UserServices(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool ValidateUser(string accessToken, string client, string uid)
        {
            var userValid = _context.UserAccess.Where(m => m.Client == client && m.AccessToken == accessToken && m.UID == uid).FirstOrDefault();

            if (userValid == null)
                return false;

            return true;
        }

        public async Task<Domain.UserAccess> SignIn(string email, string password)
        {
            var httpClient = new HttpClient();

            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(Constantes.UrlIOSYS),
                Content = new StringContent($@"{{""email"":""{email}"",""password"":""{password}""}}", Encoding.UTF8, "application/json")
            };

            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await httpClient.SendAsync(request);

            // Check login success
            if (response.IsSuccessStatusCode)
            {
                var investorData = new Domain.InvestorData();
                using (HttpContent content = response.Content)
                {
                    var json = content.ReadAsStringAsync().Result;
                    investorData = Newtonsoft.Json.JsonConvert.DeserializeObject<Domain.InvestorData>(json);
                }
                if (investorData.Success)
                {
                    var user = new UserAccess();

                    user.AccessToken = response.Headers.GetValues("access-token").FirstOrDefault();
                    user.Client = response.Headers.GetValues("client").FirstOrDefault();
                    user.UID = response.Headers.GetValues("uid").FirstOrDefault();
                    user.IdInvestor = investorData.Investor.Id;

                    // Check header is not empty
                    if (!string.IsNullOrEmpty(user.AccessToken) && !string.IsNullOrEmpty(user.Client) && !string.IsNullOrEmpty(user.UID))
                    {
                        // Incluir no banco de dados ...
                        _context.UserAccess.Add(user);
                        _context.SaveChanges();

                        return new Domain.UserAccess
                        {
                            AccessToken = user.AccessToken,
                            Client = user.Client,
                            UID = user.UID
                        };
                    }
                }
            }

            return null;
        }
    }
}
