﻿using System.Threading.Tasks;

namespace empresas_dotNET.Services
{
    public interface IUserServices
    {
        Task<Domain.UserAccess> SignIn(string email, string password);
        bool ValidateUser(string accessToken, string client, string uid);
    }
}
