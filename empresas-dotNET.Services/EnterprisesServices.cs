﻿using empresas_dotNET.DAL;
using empresas_dotNET.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace empresas_dotNET.Services
{

    public class EnterprisesServices : IEnterprisesServices
    {
        private readonly ApplicationDbContext _context;

        public EnterprisesServices(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Enterprise> Get(int? type,string name)
        {

            var asQueryable = _context.Enterprise.AsQueryable();

            if (!string.IsNullOrEmpty(name))
                asQueryable = asQueryable.Where(m => m.Name == name);

            if (type.HasValue)
                asQueryable = asQueryable.Where(m => m.Type == type);


            return asQueryable.ToList();
        }

        public Enterprise Get(int id)
        {
            return _context.Enterprise.Find(id);
        }
    }
}
