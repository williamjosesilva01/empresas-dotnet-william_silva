﻿using empresas_dotNET.DAL;
using empresas_dotNET.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;

namespace empresas_dotNET.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IEnterprisesServices _enterprisesServices;

        public EnterprisesController(ApplicationDbContext context, IEnterprisesServices enterprisesServices)
        {
            _context = context;
            _enterprisesServices = enterprisesServices;
        }

        [AuthorizeIOSYS]
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id) => Ok(_enterprisesServices.Get(id));

        [AuthorizeIOSYS]
        [HttpGet()]
        public ActionResult<IEnumerable<string>> Get(int? enterprise_types, string name) => Ok(_enterprisesServices.Get(enterprise_types, name));

        public class AuthorizeIOSYS : Attribute, IResourceFilter
        {
            public void OnResourceExecuting(ResourceExecutingContext context)
            {
                var userServices = (IUserServices)context.HttpContext.RequestServices.GetService(typeof(IUserServices));

                StringValues accessToken = new StringValues();
                context.HttpContext.Request.Headers.TryGetValue("access-token", out accessToken);

                StringValues client = new StringValues();
                context.HttpContext.Request.Headers.TryGetValue("client", out client);

                StringValues uid = new StringValues();
                context.HttpContext.Request.Headers.TryGetValue("uid", out uid);

                var userValid = userServices.ValidateUser(accessToken.FirstOrDefault(), client.FirstOrDefault(), uid.FirstOrDefault());

                if (!userValid)
                    context.Result = new UnauthorizedResult();

            }

            public void OnResourceExecuted(ResourceExecutedContext context)
            {
            }
        }
    }
}
