﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotNET.DAL;
using empresas_dotNET.Models;
using empresas_dotNET.Services;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotNET.Controllers
{
    [Route("api/v1/[controller]/auth")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IUserServices _userServices;

        public UsersController(ApplicationDbContext context, IUserServices userServices)
        {
            _context = context;
            _userServices = userServices;
        }

        [HttpPost()]
        [Route("sign_in")]
        public async Task<ActionResult> sign_in(SignInModel model)
        {
            try
            {
                var userAccess = await _userServices.SignIn(model.Email, model.Password);

                if (userAccess == null)
                    return Unauthorized();

                return Ok(userAccess);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
